package Utility;

import static io.restassured.RestAssured.given;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

/**
 * @author ${Onkar Bhagwat}
 *
 * 
 */
public class RestAssuredAPIFramework {
	static Logger logger = Logger.getLogger("RestAssuredAPIFramework");

	public static void copyFile(String source, String destination) {
		File src = new File(source);

		File dest = new File(destination);

		long start = 0;
		try {
			logger.info("File copy: In Progress");
			start = System.currentTimeMillis();
			FileUtils.copyFileToDirectory(src, dest);
			// some time passes
			long end = System.currentTimeMillis();
			long elapsedTime = end - start;
			long minutes = TimeUnit.MILLISECONDS.toMinutes(elapsedTime);
			logger.info("Total time taken to copy file is : " + minutes + "mins");
			logger.info("File successfully gets copied to remote server");
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static int getResponseStatus(String apiEndPoint) {
		int statusCode = given().when().get(apiEndPoint).getStatusCode();
		// queryParam("ProcessType","0").queryParam("MailDatImportFileID", "49")
		System.out.println("The response status is " + statusCode);
		return statusCode;
	}

	public static Response getResponse(String apiEndPoint, Map<String, ?> parametersMap) {

		RestAssured.baseURI = getServerAPIEndpoint();
		RequestSpecification httpRequest = RestAssured.given();
		// Response response = httpRequest.get(apiEndPoint);
		Response response = httpRequest.params(parametersMap).get(apiEndPoint);
		return response;

	}

	@SuppressWarnings("unused")
	private static String getServerAPIEndpoint() {

		Properties prop = new Properties();
		FileInputStream fis = null;
		try {
			fis = new FileInputStream("src//main//resources//Browser.properties");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			prop.load(fis);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return prop.getProperty("API_ENDPOINT");

	}

}
