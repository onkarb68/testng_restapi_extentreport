package Utility;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Base64;
import java.util.Properties;

import org.apache.log4j.Logger;

public class ConnectToMSSQLDatabase {
	Logger logger = Logger.getLogger("ConnectToMSSQLDatabase");

	static String DB_URL, username, password;
	static Statement stmt;
	static Connection con;
	
	//This method forms the connection to database
	public static void initiateDBConnection() {
		Properties prop = new Properties();
		FileInputStream fis = null;
		try {
			fis = new FileInputStream("src//main//resources//Browser.properties");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			prop.load(fis);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		DB_URL = prop.getProperty("CENTEROPS_DEV_DBURL");
		username = prop.getProperty("DB_USERNAME_DEV");
		password = prop.getProperty("DB_PASSWORD_DEV");

		byte[] decoded = Base64.getDecoder().decode(password);
		String decodedPassword = new String(decoded);
		try {
			con = DriverManager.getConnection(DB_URL, username, decodedPassword);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//This method returns the data for query you sent
	public static ResultSet getRecords(String getRecordsQuery) throws SQLException, ClassNotFoundException, IOException {
		initiateDBConnection();
		stmt = con.createStatement();
		// Executing the SQL Query and store the results in ResultSet
		ResultSet rs = stmt.executeQuery(getRecordsQuery);
		return rs;
	}
	
	//This method updates the data for query you sent
	public static void updateRecords(String updateRecordsQuery)
			throws SQLException, ClassNotFoundException, IOException {
		initiateDBConnection();
		stmt = con.createStatement();
		stmt.executeUpdate(updateRecordsQuery);
	}
	
	//This method deletes the data for query you sent
	public static void deleteQuery(String deleteRecordQuery) throws SQLException, ClassNotFoundException, IOException {
		initiateDBConnection();
		stmt = con.createStatement();
		stmt.executeUpdate(deleteRecordQuery);
	}
	
	//This method closes the database connection
	public static void closeDatabaseSocket() throws SQLException, ClassNotFoundException, IOException {
		initiateDBConnection();
		con.close();
	}

}
