package Utility;

import org.apache.log4j.Logger;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

/**
 * @author ${Onkar Bhagwat}
 *
 * 
 */
public class ExtentReportsUtility {

	static ExtentReports extent;
	static ExtentTest logger;
	static Logger logger1 = Logger.getLogger("ExtentReportsUtility");

	public static void initiateReportFile() {
		extent = new ExtentReports("./ExtentReport/extentReports.html", true);
	}

	public static void createTest(String testName) {
		logger = null;
		logger = extent.startTest(testName);
	}

	public static void usefulInfo(String informationMessage) {
		logger.log(LogStatus.INFO, informationMessage);
	}

	public static void markTestPass(String passMessage) {
		logger.log(LogStatus.PASS, passMessage);
	}

	public static void markTestFail(String failMessage) {
		logger.log(LogStatus.FAIL, failMessage);
	}

	public static void endTest() {
		extent.flush();
	
	}
	
	public static void attachSnapToReport( String screenshotPath) {
		logger.log(LogStatus.FAIL, logger.addScreenCapture(screenshotPath));
	}
}
