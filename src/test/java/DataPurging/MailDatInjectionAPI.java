package DataPurging;

import static Utility.ConnectToMSSQLDatabase.closeDatabaseSocket;
import static Utility.ConnectToMSSQLDatabase.getRecords;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * @author ${Onkar Bhagwat}
 *
 * 
 */
public class MailDatInjectionAPI {
	static Logger logger = Logger.getLogger("MailDatInjectionAPI");

	public static String getProperty(String propName) {
		Properties prop = new Properties();
		FileInputStream fis = null;
		try {
			fis = new FileInputStream("src//main//resources//DatabaseQuries.properties");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			prop.load(fis);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String propertyName = prop.getProperty(propName);
		return propertyName;

	}

	@SuppressWarnings("null")
	public static List<String> getLoadStatusRowQuallityIssuesDataQualityIssues() {

		String query=getProperty("getLoadStatusRowQuallityIssuesDataQualityIssues");
		logger.info("query  : " +query);
		List<String> listMailDatImportFile = new ArrayList<String>();
		try {
			ResultSet rs;
			rs = getRecords(query);

			while (rs.next()) {
				listMailDatImportFile.add(0, rs.getString("MailDatImportFileID"));
				listMailDatImportFile.add(1, rs.getString("MailDatImportFileLoadStatusTypeID"));
				listMailDatImportFile.add(2, rs.getString("RowCountDataQualityIssues"));
				listMailDatImportFile.add(3, rs.getString("RowCountDuplicates"));
				listMailDatImportFile.add(4, rs.getString("RowCountNew"));
				logger.info("Data got inserted in list from MailDatImportFile table");

			}
			closeDatabaseSocket();
		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		}

		return listMailDatImportFile;

	}

	public static boolean isTableUpdatedWithLatestTimeStamp(String query, String timeColumnName) throws Exception {

		Format f = new SimpleDateFormat("yyyy-MM-dd");
		String todaysDate = f.format(new Date());
		logger.info("Current Date = " + todaysDate);

		String latestTimestamp = null;
		try {
			ResultSet rs;
			rs = getRecords(query);
			while (rs.next()) {
				latestTimestamp = rs.getString(timeColumnName);

			}
			closeDatabaseSocket();
		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		}

		try {
			if (latestTimestamp.contains(todaysDate)) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			throw new Exception("Table is not upated with latest time stmap");
		}
	}

	public static String getQueryValueFromTable(String query) {
		String value = null;
		try {
			ResultSet rs;
			rs = getRecords(query);
			while (rs.next()) {
				value = rs.getString("");
			}
			closeDatabaseSocket();
		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		}
		return value;

	}

	public static String getAnyParticularValueFromTable(String query, String colName) {
		String value = null;
		try {
			ResultSet rs;
			rs = getRecords(query);
			while (rs.next()) {
				value = rs.getString(colName);
			}
			closeDatabaseSocket();
		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		}
		return value;

	}

	public static List<String> getLastestUnprocessedFiles() {

		String query=getProperty("getLastestUnprocessedFiles");
		logger.info("query  : " +query);
		List<String> listMailDatImportFile = new ArrayList<String>();
		try {
			ResultSet rs;
			rs = getRecords(query);

			while (rs.next()) {
				listMailDatImportFile.add(0, rs.getString("MailDatImportFileID"));
				listMailDatImportFile.add(1, rs.getString("MailDatImportFileLoadStatusTypeID"));
				listMailDatImportFile.add(2, rs.getString("RowCountDataQualityIssues"));
				listMailDatImportFile.add(3, rs.getString("RowCountDuplicates"));
				listMailDatImportFile.add(4, rs.getString("RowCountNew"));
				logger.info("Data got inserted in list from MailDatImportFile table");

			}
			closeDatabaseSocket();
		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		}

		return listMailDatImportFile;

	}

}
