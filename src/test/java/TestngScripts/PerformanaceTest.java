package TestngScripts;

import static DataPurging.MailDatInjectionAPI.getAnyParticularValueFromTable;
import static DataPurging.MailDatInjectionAPI.getLastestUnprocessedFiles;
import static DataPurging.MailDatInjectionAPI.getLoadStatusRowQuallityIssuesDataQualityIssues;
import static DataPurging.MailDatInjectionAPI.getQueryValueFromTable;
import static DataPurging.MailDatInjectionAPI.isTableUpdatedWithLatestTimeStamp;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import Utility.ExtentReportsUtility;
import Utility.RestAssuredAPIFramework;
import io.restassured.response.Response;

public class PerformanaceTest {

	public Properties prop;
	Logger logger = Logger.getLogger("PerformanaceTest");

	@BeforeClass
	static void initiateExtentReport() throws IOException {
		ExtentReportsUtility.initiateReportFile();
	}

	@Test(priority = 0, invocationCount = 10)
	public void copyeDocZip() throws InterruptedException {
		String src = "src//main//resources//_test_GOOD_SPMSPB16.zip";
		String dest = "\\\\\\\\PRSSISDC2VDBT1\\\\MailDatSFTP\\\\";

		RestAssuredAPIFramework.copyFile(src, dest);
		logger.info("Start: Waiting for Automatice scedular to copy file");
		Thread.sleep(90000);
		logger.info("Finished: Automatice scedular copied file");
	}

	@Test(priority = 1, invocationCount = 10)
	public void validateAPIForValidFile() throws Exception {
		logger.info("Start,Performanance Testing: Validate Injection API end to end for valid zip file");
		ExtentReportsUtility.createTest("Performanance testing of mailDat injection API");
		ExtentReportsUtility
				.usefulInfo("Start,Performanance Testing: Validate Injection API end to end for valid zip file");

		List<String> actulDataBeforeAPIInjection, actulDataAfterAPIInjection;
		String mailDatInjectationAPIEndPoint = "/edocingestion";

		logger.info("Start: Checking file readiness before API trigger");
		actulDataBeforeAPIInjection = getLastestUnprocessedFiles();
		String mailDatImportFileID = actulDataBeforeAPIInjection.get(0);
		logger.info("mailDatImportFileID  " + mailDatImportFileID);
		logger.info(actulDataBeforeAPIInjection);
		logger.info(actulDataBeforeAPIInjection.get(1));
		Assert.assertEquals("0", actulDataBeforeAPIInjection.get(1));
		Assert.assertEquals(null, actulDataBeforeAPIInjection.get(2));
		Assert.assertEquals(null, actulDataBeforeAPIInjection.get(3));
		Assert.assertEquals(null, actulDataBeforeAPIInjection.get(4));
		logger.info("Finished: Checking file readiness before API trigger");

		logger.info("Triggering the mailDat injection API");
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("ProcessType", "1");
		parameters.put("MailDatImportFileID", mailDatImportFileID);
		Response resp = RestAssuredAPIFramework.getResponse(mailDatInjectationAPIEndPoint, parameters);
		logger.info("Got the reponse from the mailDat injection API");
		Assert.assertEquals("File is not injected successfully", 200, resp.statusCode());
		logger.info("Body message: " + resp.asString());
		Assert.assertTrue(resp.asString().contains("Begin Ingestion Process for File"));
		Assert.assertTrue(resp.asString().contains("test_GOOD_SPMSPB16.zip"));
		Assert.assertTrue(resp.asString().contains("SUCCESS"));
		Assert.assertTrue(resp.asString().contains("Ingested"));

		logger.info("Start, After Injection: Verify the database table MailDatImportFile");
		Thread.sleep(10000);
		actulDataAfterAPIInjection = getLoadStatusRowQuallityIssuesDataQualityIssues();
		logger.info(actulDataAfterAPIInjection);
		Assert.assertEquals("2", actulDataAfterAPIInjection.get(1));
		Assert.assertEquals(null, actulDataAfterAPIInjection.get(2));
		Assert.assertEquals(null, actulDataAfterAPIInjection.get(3));
		String rowCountNew = actulDataAfterAPIInjection.get(4);

		String queryForTimeStamp = "Select Top 1 [eDocTimestamp] FROM [CenterOPS].[Operations].[eDocIngestionMailPieces] where [MailDatImportFileID]="
				+ mailDatImportFileID + "  order by [eDocTimestamp] ASC";

		Assert.assertTrue("eDocTimestamp is not updated with current date",
				isTableUpdatedWithLatestTimeStamp(queryForTimeStamp, "eDocTimestamp"));
		logger.info("Finished, After Injection: Verify the database table MailDatImportFile");

		logger.info("Start, After Injection: Verify the database table eDocIngestionMailPieceError");
		String queryForIMB = "SELECT COUNT(IMB) FROM [CenterOPS].[Operations].[eDocIngestionMailPieces] where [MailDatImportFileID]="
				+ mailDatImportFileID;
		Assert.assertEquals(rowCountNew, getQueryValueFromTable(queryForIMB));
		logger.info("Finished, After Injection: Verify the database table eDocIngestionMailPieceError");

		logger.info("Start:After Injection,Verify the database table [ByForFileForMailDat]");

		String queryForeDocIngestionMailPiecesTable = "Select Top 1 [MailOwnerMID] FROM [CenterOPS].[Operations].[eDocIngestionMailPieces] where MailDatImportFileID="
				+ mailDatImportFileID + "  order by [eDocTimestamp] ASC";
		String mailOwnerMIDFromeDocIngestionMailPiecesTable = getAnyParticularValueFromTable(
				queryForeDocIngestionMailPiecesTable, "MailOwnerMID");

		logger.info("mailOwnerMIDFromeDocIngestionMailPiecesTable: " + mailOwnerMIDFromeDocIngestionMailPiecesTable);

		String queryByForFileForMailDatTable = "SELECT TOP 1 [MailOwnerMID] FROM [CenterOPS].[ExportStaging].[ByForFileForMailDat] where MailOwnerMID=(Select Top 1 [MailOwnerMID] FROM [CenterOPS].[Operations].[eDocIngestionMailPieces] where MailDatImportFileID="
				+ "mailDatImportFileID" + "  order by [eDocTimestamp] ASC)";
		String mailOwnerMIDFromByForFileForMailDatTable = getAnyParticularValueFromTable(queryByForFileForMailDatTable,
				"MailOwnerMID");

		logger.info("mailOwnerMIDFromByForFileForMailDatTable: " + mailOwnerMIDFromeDocIngestionMailPiecesTable);

		Assert.assertEquals(mailOwnerMIDFromeDocIngestionMailPiecesTable, mailOwnerMIDFromByForFileForMailDatTable);

		logger.info("Finished:After Injection,Verify the database table [ByForFileForMailDat]");

		logger.info("Finished,Use case 1: Validate Injection API end to end for valid zip file");
		ExtentReportsUtility
				.usefulInfo("Finished,Performanance Testing: Validate Injection API end to end for valid zip file");
		ExtentReportsUtility
				.markTestPass("Use case 1: Validate Injection API end to end for valid zip file successfully");
		ExtentReportsUtility.endTest();
	}

}
