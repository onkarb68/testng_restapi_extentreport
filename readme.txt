
A. While designing framework, following very essentiAL pivotal building blocks have been developed: 
	1.Page objects
	2.Data driven testing
	3.Automated Performance Testing
	4.Database Automation
	5. Rest API Automation
	6. Extent Report
	7. Rerun of failed test
	8. Running build as a part of CICD pipeline using jenkinfile as pipeline as code
	9.GUI and API core reusable framework
	10. TestNG as Testing framework
	11. Made it very modular so that any team member would learn and contribute at scale
	
	Any value add suggestion always welcome !! :) 
	
	Please fork and create PR to proceed. Lets create amazing stuff for seamless automation
	 
	
